import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";
const Users = () => {
  const [user, setUser] = useState({
    name: "",
    username: "",
    email: "",
    phone: "",
    website: "",
  });
  const { id } = useParams();

  useEffect(() => {
    loadUser();
  }, []);

  const loadUser = async () => {
    const result = await axios.get(`http://localhost:3003/users/${id}`);
    setUser(result.data);
  };
  return (
    <div className="container py-4">
      <Link className="btn btn-primary" to="/">
        Back to Home
      </Link>
      <h1 className="display-4">UserId :{id}</h1>
      <hr />
      <ul className="list-group w-5"></ul>
      <li className="list-group-item">Name:{user.name}</li>
      <li className="list-group-item">Username:{user.username}</li>
      <li className="list-group-item">Email:{user.email}</li>
      <li className="list-group-item">phone:{user.phone}</li>
      <li className="list-group-item">Website:{user.website}</li>
    </div>
  );
};

export default Users;
